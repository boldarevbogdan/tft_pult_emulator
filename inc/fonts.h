/*
 * fonts.h
 *
 *  Created on: 11 окт. 2017 г.
 *      Author: bogdan
 */

#ifndef FONTS_H_
#define FONTS_H_

#include <stdint.h>

typedef struct
{
	uint8_t x_size;
	uint8_t y_size;
	uint8_t data_size;
	const uint8_t ** digits_array;
} DigitSize_Typedef;

extern const DigitSize_Typedef Digit_Size_Big;
extern const DigitSize_Typedef Digit_Size_Medium;
extern const DigitSize_Typedef Digit_Size_MediumSmall;
extern const DigitSize_Typedef Digit_Size_Small;

extern const uint8_t B_colon[78];
extern const uint8_t B_dot[24];

extern const uint8_t M_colon[40];
extern const uint8_t M_dot[14];

extern const uint8_t Ms_colon[32];
extern const uint8_t Ms_dot[12];
extern const uint8_t Ms_degree[24];
extern const uint8_t Ms_dash[8];

extern const uint8_t S_colon[24];
extern const uint8_t S_percent[28];

#endif /* FONTS_H_ */
