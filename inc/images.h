/*
 * pictures.h
 *
 *  Created on: 10 окт. 2017 г.
 *      Author: bogdan
 */

#ifndef IMAGES_H_
#define IMAGES_H_

#include <stdint.h>

typedef struct {
	const uint16_t * buf;
	uint16_t x_size;
	uint16_t y_size;
} Image_Typedef;



#endif /* IMAGES_H_ */
