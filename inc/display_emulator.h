/*
 * display_emulator.h
 *
 *  Created on: 10 окт. 2017 г.
 *      Author: bogdan
 */

#ifndef DISPLAY_EMULATOR_H_
#define DISPLAY_EMULATOR_H_

#include <stdint.h>
#include <GL/glut.h>
#include "images.h"
#include "fonts.h"

#define DISPLAY_WIDTH	320
#define DISPLAY_HEIGHT	240

typedef struct
{
	GLint x;
	GLint y;
} Point_Typedef;

typedef struct
{
	uint8_t R;
	uint8_t G;
	uint8_t B;
} RGB_Typedef;

typedef enum
{
	HORIZONTAL = 0,
	VERTICAL
} LineDirection_Typedef;


/* залить весь экран цветом */
void Fill(uint16_t color);

/*
 * Залить область цветом
 */
void FillArea(uint16_t x_pos, uint16_t y_pos, uint16_t x_size, uint16_t y_size, uint16_t color);

/*
 * Нарисовать линию в координатах x_pos, y_pos,
 */
void DrawLine(uint16_t x_pos, uint16_t y_pos, uint16_t length,
		LineDirection_Typedef direction, uint16_t color, uint8_t thickness = 1);

/*
 * Вывести на экран иконку
 */
void PutIcon(const Image_Typedef * image, uint16_t x_pos, uint16_t y_pos);

/*
 * Вставить на экран картинку
 */
void PutBackground(const Image_Typedef * image);

/* вывести иконку bitmap */
void PutBitmap (uint16_t x_pos, uint16_t y_pos, uint16_t color, uint16_t background_color, const uint16_t * alpha_colors,
				const uint16_t * bitmap, uint8_t bmp_size_x, uint8_t bmp_size_y);

/*
 * Вывести цифру цветом color с цветом заднего плана background_color шрифтом font
 * alpha_colors - массив из трех элементов в котором отображены переходные цвета между фоном и цветом цифры
 */
void PutDigit (uint8_t digit, uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * font);


#endif /* DISPLAY_EMULATOR_H_ */
