/*
 * graphics.h
 *
 * Файл содержит более высокоуровневые функции для вывода элементов на экран
 */

#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include "display_emulator.h"

/*
 * Вывести 3,2,1-значное число
 * Функиция возвращает значение координаты Х после выведеного числа
 * параметр lead_zero значит. если true то значение 5 отобразится как 05, если false тогда просто 5
 */
uint16_t PutNumber (uint16_t number, uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * font, bool lead_zero);

/* Отобразить часы */
void PutCurrentTime (uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color,  const uint16_t * alpha_colors, const DigitSize_Typedef * clock_size);

/* отобразить дату в формате день.месяц.год */
void PutCurrentDate (uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * date_size);

#endif /* GRAPHICS_H_ */
