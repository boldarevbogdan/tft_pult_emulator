/*
 * menu.h
 *
 *  Created on: 9 окт. 2017 г.
 *      Author: bogdan
 */

#ifndef MENU_H_
#define MENU_H_

#include <stdint.h>
#include "display_emulator.h"

typedef struct {
	uint8_t (*KeyPressResponse) (Point_Typedef *pP);
	void (*DrawMenu) (void);
} Menu_Struct_Typedef;

extern Menu_Struct_Typedef *CurrentMenu;

extern uint8_t refresh_on_click;
extern uint8_t refresh_time_expired;
extern bool button_was_pressed;

void MenuInit (void);

#endif /* MENU_H_ */
