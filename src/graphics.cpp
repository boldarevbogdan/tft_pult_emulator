/*
 * graphics.cpp
 *
 *  Created on: 10 июл. 2018 г.
 *      Author: bogdan
 */
#include "graphics.h"


uint16_t PutNumber (uint16_t number, uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * font, bool lead_zero)
{
	uint8_t shift = 0;
	uint8_t hundreds = number / 100;
	uint8_t tens = (number % 100) / 10;
	uint8_t ones = number % 10;

	if (font == &Digit_Size_Big)
		shift = 34;
	else if (font == &Digit_Size_Medium)
		shift = 18;
	else if (font == &Digit_Size_MediumSmall)
		shift = 14;
	else if (font == &Digit_Size_Small)
		shift = 11;

	if (hundreds != 0)
	{
		PutDigit (hundreds, x_pos, y_pos, color, background_color, alpha_colors, font);
		x_pos += shift;
	}

	if (lead_zero == true)
	{
		PutDigit (tens, x_pos, y_pos, color, background_color, alpha_colors, font);
		x_pos += shift;
	}
	else
	{
		if ((tens != 0) || (hundreds != 0))
		{
			PutDigit (tens, x_pos, y_pos, color, background_color, alpha_colors, font);
			x_pos += shift;
		}
	}

	PutDigit (ones, x_pos, y_pos, color, background_color, alpha_colors, font);

	return (x_pos + shift);
}

void PutCurrentTime (uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color,  const uint16_t * alpha_colors, const DigitSize_Typedef * clock_size)
{
	uint16_t new_x_pos = 0;
	uint8_t shift = (clock_size->x_size / 2);

	new_x_pos = PutNumber (0, x_pos, y_pos, color, background_color, alpha_colors, clock_size, true);

	PutNumber (0, (new_x_pos + shift), y_pos, color, background_color, alpha_colors, clock_size, true);
}

void PutCurrentDate (uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * date_size)
{
	uint16_t new_x_pos = 0;
	uint8_t shift = 0;
	uint8_t years_hunderds = 0 / 100;
	uint8_t years = 0 % 100;

	if (date_size == &Digit_Size_Big)
		shift = 0;
	else if (date_size == &Digit_Size_Medium)
		shift = 0;
	else if (date_size == &Digit_Size_MediumSmall)
		shift = 4;
	else if (date_size == &Digit_Size_Small)
		shift = 4;

	new_x_pos = PutNumber (0, x_pos, y_pos, color, background_color, alpha_colors, date_size, true);

	new_x_pos = PutNumber (0, (new_x_pos + shift), y_pos, color, background_color, alpha_colors, date_size, true);

	new_x_pos = PutNumber (years_hunderds, (new_x_pos + shift), y_pos, color, background_color, alpha_colors, date_size, true);
	PutNumber (years, new_x_pos, y_pos, color, background_color, alpha_colors, date_size, true);
}



