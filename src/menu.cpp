#include "menu.h"

uint8_t refresh_on_click;
uint8_t refresh_time_expired;
bool button_was_pressed;

uint8_t EmptyKeyPressResponseFunc (Point_Typedef *pP);
void EmptyDrawFunc (void);
void MakeMenu(Menu_Struct_Typedef * Menu_Name, uint8_t (*kpr)(Point_Typedef * pP),
		void (*dm)(void));

Menu_Struct_Typedef FirstMenu;
Menu_Struct_Typedef *CurrentMenu;

void MenuInit (void)
{
	MakeMenu (&FirstMenu, &EmptyKeyPressResponseFunc, &EmptyDrawFunc);

	CurrentMenu = &FirstMenu;
}

void MakeMenu(Menu_Struct_Typedef * Menu_Name, uint8_t (*kpr)(Point_Typedef * pP),
		void (*dm)(void))
{
	Menu_Name->KeyPressResponse = kpr;
	Menu_Name->DrawMenu = dm;
}

uint8_t EmptyKeyPressResponseFunc (Point_Typedef *pP)
{
	return 1;
}

void EmptyDrawFunc (void)
{
	return;
}
