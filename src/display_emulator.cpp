//============================================================================
// Name        : tft_display_emulator.cpp
// Author      : Bogdan
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <images.h>
#include <iostream>
#include "display_emulator.h"
#include "menu.h"
#include <stdbool.h>

using namespace std;

RGB_Typedef RGB565ToRGB888(uint16_t rgb565);

Point_Typedef p;

void MouseFunc(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		p.x = x;
		p.y = y;

		refresh_on_click = CurrentMenu->KeyPressResponse(&p);

		if (refresh_on_click)
		{
			cerr << "Touch was detected. Coordinates: x = " << p.x << ", y = " << p.y << endl;
			button_was_pressed = true;
			glutPostRedisplay();
		}
	}

	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		if (button_was_pressed)
		{
			button_was_pressed = false;
			cerr << "Touch was released." << endl;
		}
	}
}

void TimerFunc (int a)
{
	refresh_time_expired = 1;
	glutPostRedisplay();
    glutTimerFunc(500, &TimerFunc, 0);
}

void DisplayFunc(void)
{
	CurrentMenu->DrawMenu();
	cerr << "DisplayFunc was invoked" << endl;
	refresh_time_expired = 0;
	refresh_on_click = 0;
}

int main(int argc, char *argv[])
{
	MenuInit();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(800, 300);
	glutInitWindowSize(DISPLAY_WIDTH, DISPLAY_HEIGHT);
	glutCreateWindow("TFT Display");
	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
	glutDisplayFunc(DisplayFunc); /* call this function after creating window */
	glutMouseFunc(MouseFunc); /* call this function after creating window */
	glOrtho(0.0f, DISPLAY_WIDTH, DISPLAY_HEIGHT, 0.0f, -1.0, 1.0);
	glutTimerFunc(500, &TimerFunc, 0);
	glutMainLoop();

	return 0;
}

void Fill(uint16_t color)
{
	RGB_Typedef RGB =
	{ 0 };

	RGB = RGB565ToRGB888(color);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3ub(RGB.R, RGB.G, RGB.B);

	glBegin(GL_QUADS);

	glVertex2i(0, 0);
	glVertex2i(0, DISPLAY_HEIGHT);
	glVertex2i(DISPLAY_WIDTH, DISPLAY_HEIGHT);
	glVertex2i(DISPLAY_WIDTH, 0);

	glEnd();
	glFlush();
}

void FillArea(uint16_t x_pos, uint16_t y_pos, uint16_t x_size,
		uint16_t y_size, uint16_t color)
{
	RGB_Typedef RGB =
	{ 0 };

	RGB = RGB565ToRGB888(color);

	glColor3ub(RGB.R, RGB.G, RGB.B);

	glBegin(GL_QUADS);

	glVertex2i(x_pos, y_pos);
	glVertex2i(x_pos, (y_pos + y_size));
	glVertex2i((x_pos + x_size), (y_pos + y_size));
	glVertex2i((x_pos + x_size), y_pos);

	glEnd();
	glFlush();
}

void DrawLine(uint16_t x_pos, uint16_t y_pos, uint16_t length,
		LineDirection_Typedef direction, uint16_t color, uint8_t thickness)
{
	RGB_Typedef RGB =
	{ 0 };
	uint16_t x_size = 0, y_size = 0;

	if (direction == HORIZONTAL)
	{
		x_size = length;
		y_size = thickness;
	}
	else if (direction == VERTICAL)
	{
		x_size = thickness;
		y_size = length;
	}

	RGB = RGB565ToRGB888(color);

	glColor3ub(RGB.R, RGB.G, RGB.B);

	glBegin(GL_QUADS);

	glVertex2i(x_pos, y_pos);
	glVertex2i(x_pos, (y_pos + y_size));
	glVertex2i((x_pos + x_size), (y_pos + y_size));
	glVertex2i((x_pos + x_size), y_pos);

	glEnd();
	glFlush();

	glColor3ub(RGB.R, RGB.G, RGB.B);
}

void PutIcon(const Image_Typedef * image, uint16_t x_pos, uint16_t y_pos)
{
	uint32_t num_bytes = image->x_size * image->y_size;
	uint16_t * pbuf = new uint16_t[num_bytes];

	GLint y = y_pos + image->y_size;

	if (pbuf == NULL)
		return;

	for (uint16_t i = 0; i < image->y_size; i++)
	{
		for (uint16_t j = 0; j < image->x_size; j++)
			pbuf[j + (i * image->x_size)] = image->buf[j
					+ ((num_bytes) - ((i + 1) * image->x_size))];
	}

	glRasterPos2i(x_pos, y);
	glDrawPixels(image->x_size, image->y_size, GL_RGB,
				GL_UNSIGNED_SHORT_5_6_5, pbuf);
	glFlush();

	delete[] pbuf;
}

void PutBackground(const Image_Typedef * image)
{
	uint32_t num_bytes = image->x_size * image->y_size;
	uint16_t * pbuf = new uint16_t[num_bytes];

	if (pbuf == NULL)
		return;

	for (uint16_t i = 0; i < image->y_size; i++)
	{
		for (uint16_t j = 0; j < image->x_size; j++)
			pbuf[j + (i * image->x_size)] = image->buf[j
					+ ((num_bytes) - ((i + 1) * image->x_size))];
	}

	glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2i(0, DISPLAY_HEIGHT);
	glDrawPixels(image->x_size, image->y_size, GL_RGB,
				GL_UNSIGNED_SHORT_5_6_5, pbuf);
	glFlush();

	delete[] pbuf;
}

void PutBitmap (uint16_t x_pos, uint16_t y_pos, uint16_t color, uint16_t background_color, const uint16_t * alpha_colors,
				const uint16_t * bitmap, uint8_t bmp_size_x, uint8_t bmp_size_y)
{
	RGB_Typedef RGB_color = { 0 };

	RGB_color = RGB565ToRGB888(color);

	uint8_t columns_in_row = bmp_size_x / 8;		/* количество байт в одном ряду */
	uint32_t num_bytes = columns_in_row * bmp_size_y;
	uint8_t * pbuf = new uint8_t[num_bytes];

	for (uint16_t row = 0; row < bmp_size_y; row++)
	{
		for (uint16_t column = 0; column < columns_in_row; column++)
		{
			pbuf[column + (row * columns_in_row)] =
					*(bitmap + column + (num_bytes - (columns_in_row) * (row + 1)));
		}
	}

	glColor3ub(RGB_color.R, RGB_color.G, RGB_color.B);
	glRasterPos2i(x_pos, (y_pos + bmp_size_y));
	glBitmap (bmp_size_x, bmp_size_y, 0, 0, 0, 0, pbuf);
	glFlush();

	delete[] pbuf;
}

void PutDigit (uint8_t digit, uint16_t x_pos, uint16_t y_pos, uint16_t color,
		uint16_t background_color, const uint16_t * alpha_colors, const DigitSize_Typedef * font)
{
	RGB_Typedef RGB_color = { 0 };
	uint8_t columns_in_row = font->x_size / font->data_size;		/* количество байт в одном ряду */
	uint32_t num_bytes = columns_in_row * font->y_size;
	uint8_t * pbuf = new uint8_t[num_bytes];

	for (uint16_t row = 0; row < font->y_size; row++)
	{
		for (uint16_t column = 0; column < columns_in_row; column++)
		{
			pbuf[column + (row * columns_in_row)] =
					*(font->digits_array[digit] + column + (num_bytes - (columns_in_row) * (row + 1)));
		}
	}

	RGB_color = RGB565ToRGB888(color);

	glColor3ub(RGB_color.R, RGB_color.G, RGB_color.B);
	glRasterPos2i(x_pos, (y_pos + font->y_size));
	glBitmap (font->x_size, font->y_size, 0, 0, 0, 0, pbuf);
	glFlush();

	delete[] pbuf;
}

RGB_Typedef RGB565ToRGB888(uint16_t rgb565)
{
	RGB_Typedef RGB =
	{ 0 };

	RGB.R = (uint8_t) ((rgb565 >> 8) & (~0x07));
	if (RGB.R & 0x80)
		RGB.R |= 0x07;

	RGB.G = (uint8_t) ((rgb565 >> 3) & (~0x03));
	if (RGB.G & 0x80)
		RGB.G |= 0x03;

	RGB.B = (uint8_t) (rgb565 << 3);
	if (RGB.B & 0x80)
		RGB.B |= 0x07;

	return RGB;
}

